﻿using Rg.Plugins.Popup.Extensions;
using SeeZeero.UI.Models;
using SeeZeero.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SeeZeero.UI.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        HomePageViewModel viewModel;
        public HomePage()
        {
            this.BindingContext = viewModel = new HomePageViewModel(Navigation);
            InitializeComponent();

            MessagingCenter.Subscribe<CommonViewModel, object>(this, "NavigateWithData", async (sender, pageData) =>
            {
                if (Navigation.ModalStack.Count > 0)
                    await Navigation.PopToRootAsync();
                if (mainContent.Children.Count > 0)
                    mainContent.Children.RemoveAt(0);
                mainContent.Children.Add(new MyDocuments());
               await viewModel.ShowUserFiles(pageData as UserFolder);
            });
            MessagingCenter.Subscribe<CommonViewModel, PageNames>(this, "Navigate", async (sender, pageName) =>
            {
                if (Navigation.ModalStack.Count > 0)
                    await Navigation.PopToRootAsync();
                if (pageName != PageNames.ShareQRCode && mainContent.Children.Count > 0)
                    mainContent.Children.RemoveAt(0);
                switch (pageName)
                {

                    case PageNames.MyDocuments:
                        mainContent.Children.Add(new UserFoldersView());
                        break;
                    case PageNames.MyConnections:
                        mainContent.Children.Add(new MyConnections());
                        break;
                    case PageNames.ShareQRCode:
                        await Navigation.PushPopupAsync(new MyQRCode(), true);
                        break;
                    case PageNames.Notifications:
                        mainContent.Children.Add(new MyNotifications());
                        break;
                    default:
                        break;
                }
            });
        }
    }
}
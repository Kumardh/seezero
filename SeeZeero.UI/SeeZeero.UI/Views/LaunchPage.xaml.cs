﻿using Rg.Plugins.Popup.Extensions;
using SeeZeero.UI.Models;
using SeeZeero.UI.ViewModels;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SeeZeero.UI.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LaunchPage : ContentPage
    {
        public LaunchPage()
        {
            this.BindingContext = new LaunchPadViewModel(Navigation);
            InitializeComponent();
            int timer = 0;
            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
            {
                timer++;
                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (timer == 3)
                    {
                        await Navigation.PushAsync(new HomePage(), false);
                    }
                });
                if (timer == 5)
                    return false;
                return true;
            });

            MessagingCenter.Subscribe<CommonViewModel, object>(this, "DisplayActionSheet", async (sender, pageData) =>
            {
                await Navigation.PushPopupAsync(new FileOptions(pageData as UserFiles));
            });
            MessagingCenter.Subscribe<FileOptionsViewModel, object>(this, "DisplayFileDetailAlert", async (sender, pageData) =>
            {
                await Navigation.PushPopupAsync(new FileDetail(pageData as UserFiles));
            });
            MessagingCenter.Subscribe<FileOptionsViewModel, object>(this, "DisplayFileShareAlert", async (sender, pageData) =>
            {
                await Navigation.PushPopupAsync(new FileShare());
            });
            MessagingCenter.Subscribe<CommonViewModel, object>(this, "DisplayFileShareAlert1", async (sender, pageData) =>
            {
                await Navigation.PushPopupAsync(new FileShare());
            });
            MessagingCenter.Subscribe<CommonViewModel, object>(this, "NavigateWithData1", async (sender, pageData) =>
            {
                await Navigation.PushAsync(new MyFoldersPage(pageData as UserFolder));
            });
            MessagingCenter.Subscribe<CommonViewModel, PageNames>(this, "Navigate1", async (sender, pageName) =>
          {
              if (Navigation.ModalStack.Count > 0)
                  await Navigation.PopToRootAsync();
              switch (pageName)
              {

                  case PageNames.MyDocuments:
                      await Navigation.PushAsync(new HomePage(), true);
                      break;
                  case PageNames.MyConnections:
                      await Navigation.PushAsync(new ConnectionsPage(), false);
                      break;
                  case PageNames.ShareQRCode:
                      await Navigation.PushPopupAsync(new MyQRCode(), true);
                      break;
                  case PageNames.Notifications:
                      await Navigation.PushAsync(new NotificationsPage(), false);
                      break;
                  default:
                      break;
              }
          });
        }
    }
}
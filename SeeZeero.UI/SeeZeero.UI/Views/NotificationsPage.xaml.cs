﻿using SeeZeero.UI.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SeeZeero.UI.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotificationsPage : ContentPage
    {
        public NotificationsPage()
        {
            this.BindingContext = new NotificationViewModel(Navigation);
            InitializeComponent();
        }
    }
}
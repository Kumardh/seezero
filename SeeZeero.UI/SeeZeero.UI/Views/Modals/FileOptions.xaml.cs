﻿using SeeZeero.UI.Models;
using SeeZeero.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SeeZeero.UI.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FileOptions : Rg.Plugins.Popup.Pages.PopupPage
    {
        public FileOptions()
        {
            this.BindingContext = new FileOptionsViewModel(Navigation);
            InitializeComponent();
        }
        public FileOptions(UserFiles userFile):this()
        {
            var vm = this.BindingContext as FileOptionsViewModel;
            vm.FileData = userFile;
        }
    }
}
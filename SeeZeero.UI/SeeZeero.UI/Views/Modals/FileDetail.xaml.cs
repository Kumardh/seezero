﻿using SeeZeero.UI.Models;
using SeeZeero.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SeeZeero.UI.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FileDetail : Rg.Plugins.Popup.Pages.PopupPage
    {
        public FileDetail()
        {
            this.BindingContext = new FileDetailViewModel(Navigation);
            InitializeComponent();
        }
        public FileDetail(UserFiles userFile) : this()
        {
            var vm = this.BindingContext as FileDetailViewModel;
            vm.FileData = userFile;
        }
    }
}
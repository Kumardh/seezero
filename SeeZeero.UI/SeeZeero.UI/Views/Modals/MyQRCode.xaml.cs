﻿using Xamarin.Forms.Xaml;

namespace SeeZeero.UI.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyQRCode : Rg.Plugins.Popup.Pages.PopupPage
    {
        public MyQRCode()
        {
            InitializeComponent();
        }
    }
}
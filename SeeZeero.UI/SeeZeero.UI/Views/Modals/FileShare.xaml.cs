﻿using SeeZeero.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SeeZeero.UI.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FileShare : Rg.Plugins.Popup.Pages.PopupPage
    {
        public FileShare()
        {
            this.BindingContext = new FileShareViewModel(Navigation);
            InitializeComponent();
        }
    }
}
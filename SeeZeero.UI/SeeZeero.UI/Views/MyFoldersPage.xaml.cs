﻿using SeeZeero.UI.Models;
using SeeZeero.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SeeZeero.UI.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyFoldersPage : ContentPage
    {
        public MyFoldersPage()
        {
            this.BindingContext = new MyFoldersViewModel(Navigation);
            InitializeComponent();
        }
        public MyFoldersPage(UserFolder userFolder) : this()
        {
            var vm = this.BindingContext as MyFoldersViewModel;
            vm.ShowUserFiles(userFolder);
        }
        
    }
}
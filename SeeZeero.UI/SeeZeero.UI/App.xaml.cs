﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SeeZeero.UI.Services;
using SeeZeero.UI.Views;

namespace SeeZeero.UI
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();
            DependencyService.Register<MockDataStore>();
            MainPage = new NavigationPage(new LaunchPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}

﻿using Xamarin.Forms;

namespace SeeZeero.UI.ViewModels
{
    public class LaunchPadViewModel : BaseViewModel
    {
        string qRCodeImage;
        public string QRCodeImage { get { return qRCodeImage; } set { SetProperty(ref qRCodeImage, value); } }
        INavigation _navigationPage;
        public LaunchPadViewModel(INavigation navigationPage)
        {
            _navigationPage = navigationPage;
        }
    }
}

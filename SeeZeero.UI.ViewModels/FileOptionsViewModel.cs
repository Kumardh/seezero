﻿using SeeZeero.UI.Models;
using System.Windows.Input;
using Xamarin.Forms;
using Rg.Plugins.Popup.Extensions;
namespace SeeZeero.UI.ViewModels
{
    public class FileOptionsViewModel : BaseViewModel
    {
        private INavigation _navigationPage;

        public ICommand OnViewDetail { get; set; }
        public ICommand OnCloseView { get; set; }
        public ICommand OnShareFile { get; set; }
        public ICommand OnDeleteFile { get; set; }

        public UserFiles FileData { get; set; }
        public FileOptionsViewModel(INavigation navigationPage)
        {
            _navigationPage = navigationPage;
            OnViewDetail = new Command(async () =>
            {
                await _navigationPage.PopAllPopupAsync();
                MessagingCenter.Send<FileOptionsViewModel, object>(this, "DisplayFileDetailAlert", FileData);

            });
            OnCloseView = new Command(async () =>
            {
                await _navigationPage.PopAllPopupAsync();
            });
            OnShareFile = new Command(async () =>
            {
                await _navigationPage.PopAllPopupAsync();
                MessagingCenter.Send<FileOptionsViewModel, object>(this, "DisplayFileShareAlert", FileData);
            });
            OnDeleteFile = new Command(async () =>
            {
                await _navigationPage.PopAllPopupAsync();
            });
        }

    }
}

﻿using SeeZeero.UI.Models;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace SeeZeero.UI.ViewModels
{
	public class HomePageViewModel : CommonViewModel
	{
		#region Collection Proprties
		private ObservableCollection<UserFolder> userFolder;
		public ObservableCollection<UserFolder> FoldersList
		{
			get { return userFolder; }
			set { SetProperty(ref userFolder, value); }
		}

		private ObservableCollection<Connections> connectionsList;
		public ObservableCollection<Connections> ConnectionsList
		{
			get { return connectionsList; }
			set { SetProperty(ref connectionsList, value); }
		}

		private ObservableCollection<Notifications> notificationsList;
		public ObservableCollection<Notifications> NotificationsList
		{
			get { return notificationsList; }
			set { SetProperty(ref notificationsList, value); }
		}

		private ObservableCollection<UserFiles> filesList;
		public ObservableCollection<UserFiles> FilesList
		{
			get { return filesList; }
			set { SetProperty(ref filesList, value); }
		} 
		#endregion

		string folderOwnerName;
		public string FolderName { get { return folderOwnerName; } set { SetProperty(ref folderOwnerName, value); } }


		private string folderDescription;

		public string FolderDescription
		{
			get { return folderDescription; }
			set { SetProperty(ref folderDescription, value); }
		}

		public ICommand OnFileSelect { get; set; }

		public ICommand OnFolderSelect { get; set; }
		INavigation _navigationPage;
		public HomePageViewModel(INavigation navigationPage) : base(navigationPage)
		{
			_navigationPage = navigationPage;
			
			OnFolderSelect = new Command<UserFolder>((userFile) =>
			{
				var selectedFile = userFile;
				MessagingCenter.Send<CommonViewModel, object>(this, "NavigateWithData", userFile);
			});
			OnFileSelect = new Command<UserFiles>((userFile) =>
			{
				var selectedFile = userFile;
				MessagingCenter.Send<CommonViewModel, object>(this, "DisplayActionSheet", userFile);

			});
			FillSampleData();
		}

		void FillSampleData()
		{
			FoldersList = new ObservableCollection<UserFolder>
			{
				new UserFolder
				{
					FolderName="My Folder",
					UserType = UserType.SELF
				},
				new UserFolder
				{
					FolderName="Harry",
					UserType = UserType.OTHER
				},
				new UserFolder
				{
					FolderName="Jack",
					UserType = UserType.OTHER
				},
				new UserFolder
				{
					FolderName="David",
					UserType = UserType.OTHER
				},
				new UserFolder
				{
					FolderName="User 1",
					UserType = UserType.OTHER
				},
				new UserFolder
				{
					FolderName="User 2",
					UserType = UserType.OTHER
				},
				new UserFolder
				{
					FolderName="User 3",
					UserType = UserType.OTHER
				},
				new UserFolder
				{
					FolderName="User 4",
					UserType = UserType.OTHER
				},
				new UserFolder
				{
					FolderName="User 5",
					UserType = UserType.OTHER
				},
			};
			ConnectionsList = new ObservableCollection<Connections>
			{
				new Connections
				{
					ConnectionIcon = "Assets/user.png",
					ConnectionName = "Nivin Newberg",
					RequestButton = "Request Sent",
					IsButton = true,
					IsMore =false
				},
				new Connections
				{
					ConnectionIcon = "Assets/user.png",
					ConnectionName = "Terry Root",
					RequestButton = "Request Sent",
					IsButton = true,
					IsMore =false
				},
				new Connections
				{
					ConnectionIcon = "Assets/user.png",
					ConnectionName = "Mike Thomas",
					IsButton = false,
					IsMore =true
				},
				new Connections
				{
					ConnectionIcon = "Assets/user.png",
					ConnectionName = "Sharon Neal",
					IsButton = false,
					IsMore =true
				},
				new Connections
				{
					ConnectionIcon = "Assets/user.png",
					ConnectionName = "Matt Kramer",
					IsButton = false,
					IsMore =true
				},
			};
			NotificationsList = new ObservableCollection<Notifications>
			{
				new Notifications
				{
					ConnectionName = "Renaldo Rozman",
					ConnectionStatus = ConnectionStatus.RECIEVED
				},
				 new Notifications
				{
					ConnectionName = "Elene Jeppesen",
					ConnectionStatus = ConnectionStatus.ACCEPTED
				},
				  new Notifications
				{
					ConnectionName = "Kimiko Hoyle",
					ConnectionStatus = ConnectionStatus.REJECTED
				},
				   new Notifications
				{
					ConnectionName = "Argellia",
					ConnectionStatus = ConnectionStatus.EXPIRED
				},
					new Notifications
				{
					ConnectionName = "Joey Gumm",
					ConnectionStatus = ConnectionStatus.FILE_SHARED
				},
			};

		}

		public async Task<ObservableCollection<UserFiles>> ShowUserFiles(UserFolder userFolder)
		{
			FolderName = (userFolder.UserType == UserType.SELF ? "My " : userFolder.FolderName + "'s ") + "Folder";
			FolderDescription = "Edocs " + (userFolder.UserType == UserType.SELF ? "uploaded by you " : " Sent by " + userFolder.FolderName);
			FilesList = new ObservableCollection<UserFiles>
			{
				new UserFiles
				{
					FileName="Resume.pdf",
					FileType = FileType.PDF,
					FileDate = DateTime.Now.AddDays(-3),
					SharedBy ="Test User",
					SharedTo ="Test User 1",
					FileSize="10.3 MB"
				},
				new UserFiles
				{
					FileName="Project Report.pdf",
					FileType = FileType.PDF,
					FileDate = DateTime.Now.AddDays(-5),
					SharedBy ="Test User 2",
					SharedTo ="Test User 11",
					FileSize="110.65 MB"
				},
				new UserFiles
				{
					FileName="Suggestions.docx",
					FileType = FileType.MS_WORD,
					FileDate = DateTime.Now.AddDays(-20),
					SharedBy ="Test User",
					SharedTo ="Test User 1",
					FileSize="10.3 MB"
				},
				new UserFiles
				{
					FileName="Company Presentation.pptx",
					FileType = FileType.MS_PPT,
					FileDate = DateTime.Now.AddDays(-25),
						SharedBy ="Test User",
					SharedTo ="Test User 1",
					FileSize="10.3 MB"
				},

				new UserFiles
				{
					FileName="Website Data.doc",
					FileType = FileType.MS_WORD,
					FileDate = DateTime.Now.AddDays(-5),
						SharedBy ="Test User",
					SharedTo ="Test User 1",
					FileSize="10.3 MB"
				},
				new UserFiles
				{
					FileName="Passport Photo.png",
					FileType = FileType.IMAGE,
					FileDate = DateTime.Now.AddDays(-5),
						SharedBy ="Test User",
					SharedTo ="Test User 1",
					FileSize="10.3 MB"
				},
			};
			return await Task.FromResult<ObservableCollection<UserFiles>>(FilesList);
		}
	}
}

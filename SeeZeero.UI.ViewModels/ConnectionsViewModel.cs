﻿using SeeZeero.UI.Models;
using System.Collections.ObjectModel;
using System.Threading;
using Xamarin.Forms;

namespace SeeZeero.UI.ViewModels
{
    public class ConnectionsViewModel : CommonViewModel
    {
        INavigation _navigationPage;
        private ObservableCollection<Connections> connectionsList;

        public ObservableCollection<Connections> ConnectionsList
        {
            get { return connectionsList; }
            set { SetProperty(ref connectionsList, value); }
        }

        public ConnectionsViewModel(INavigation navigationPage) : base(navigationPage)
        {
            _navigationPage = navigationPage;
            Device.BeginInvokeOnMainThread(() =>
            {
                Thread.SpinWait(10);
                ConnectionsList = new ObservableCollection<Connections>
            {
                new Connections
                {
                    ConnectionIcon = "Assets/user.png",
                    ConnectionName = "Nivin Newberg",
                    RequestButton = "Request Sent",
                    IsButton = true,
                    IsMore =false
                },
                new Connections
                {
                    ConnectionIcon = "Assets/user.png",
                    ConnectionName = "Terry Root",
                    RequestButton = "Request Sent",
                    IsButton = true,
                    IsMore =false
                },
                new Connections
                {
                    ConnectionIcon = "Assets/user.png",
                    ConnectionName = "Mike Thomas",
                    IsButton = false,
                    IsMore =true
                },
                new Connections
                {
                    ConnectionIcon = "Assets/user.png",
                    ConnectionName = "Sharon Neal",
                    IsButton = false,
                    IsMore =true
                },
                new Connections
                {
                    ConnectionIcon = "Assets/user.png",
                    ConnectionName = "Matt Kramer",
                    IsButton = false,
                    IsMore =true
                },
            };
            }
            );

        }
    }
}

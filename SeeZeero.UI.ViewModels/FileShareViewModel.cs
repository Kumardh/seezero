﻿using Rg.Plugins.Popup.Extensions;
using SeeZeero.UI.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace SeeZeero.UI.ViewModels
{
    public class FileShareViewModel : BaseViewModel
    {
        public ICommand OnCloseView { get; set; }

        INavigation _navigationPage;
        private ObservableCollection<UserDetail> usersList;
        public ObservableCollection<UserDetail> UsersList
        {
            get { return usersList; }
            set { SetProperty(ref usersList, value); }
        }
        private List<UserDetail> usersFilteredList;
        public List<UserDetail> UsersFilteredList
        {
            get { return usersFilteredList; }
            set { SetProperty(ref usersFilteredList, value); }
        }
        private string searcText;

        public string SearchText
        {
            get { return searcText; }
            set { SetProperty(ref searcText, value); FilterUsers(); }
        }
        void FilterUsers()
        {
            if (!string.IsNullOrWhiteSpace(SearchText))
            {
                UsersFilteredList = UsersList.Where(m => m.FullName.Contains(SearchText)).ToList();
            }
            else
            {
                UsersFilteredList = UsersList.ToList();
            }
        }
        public FileShareViewModel(INavigation navigation)
        {
            _navigationPage = navigation;
            UsersList = new ObservableCollection<UserDetail>
            {
                new UserDetail
                {
                    FullName = "Andrew Schultz"
                },
                 new UserDetail
                {
                    FullName = "Bill Wade"
                },
                  new UserDetail
                {
                      FullName ="Charles Greggory"
                },
                   new UserDetail
                {
                    FullName="User LName"
                },
                    new UserDetail
                {
                    FullName="Matt Kramer"
                },
                     new UserDetail
                {
                        FullName="Dharmendra Kumar"
                },
            };
            FilterUsers();
            OnCloseView = new Command(async () =>
          {
              await _navigationPage.PopAllPopupAsync();
          });

        }
    }
}

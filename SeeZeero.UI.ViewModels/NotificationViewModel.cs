﻿using SeeZeero.UI.Models;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace SeeZeero.UI.ViewModels
{
    public class NotificationViewModel : CommonViewModel
    {
        private ObservableCollection<Notifications>  notificationsList;

        public ObservableCollection<Notifications> NotificationsList
        {
            get { return notificationsList; }
            set { SetProperty(ref notificationsList, value); }
        }
        INavigation _navigationPage;
        public NotificationViewModel(INavigation navigationPage) : base(navigationPage)
		{
			_navigationPage = navigationPage;
            NotificationsList = new ObservableCollection<Notifications>
            {
                new Notifications
                {
                    ConnectionName = "Renaldo Rozman",
                    ConnectionStatus = ConnectionStatus.RECIEVED
                },
                 new Notifications
                {
                    ConnectionName = "Elene Jeppesen",
                    ConnectionStatus = ConnectionStatus.ACCEPTED
                },
                  new Notifications
                {
                    ConnectionName = "Kimiko Hoyle",
                    ConnectionStatus = ConnectionStatus.REJECTED
                },
                   new Notifications
                {
                    ConnectionName = "Argellia",
                    ConnectionStatus = ConnectionStatus.EXPIRED
                },
                    new Notifications
                {
                    ConnectionName = "Joey Gumm",
                    ConnectionStatus = ConnectionStatus.FILE_SHARED
                },
            };
        }
    }
}

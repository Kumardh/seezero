﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

using SeeZeero.UI.Models;
using SeeZeero.UI.Services;

namespace SeeZeero.UI.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public IDataStore<UserFiles> DataStore => DependencyService.Get<IDataStore<UserFiles>>();

        bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        string copyrightText;
        public string CopyrightText
        {
            get { return copyrightText; }
            set { SetProperty(ref copyrightText, value); }
        }
        string headingText;
        public string HeadingText
        {
            get { return headingText; }
            set { SetProperty(ref headingText, value); }
        }
        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public BaseViewModel()
        {
            CopyrightText = "© Copyright 2020 All Rights Reserved";
            HeadingText = "See Zero";
        }
    }
}

﻿using Rg.Plugins.Popup.Extensions;
using SeeZeero.UI.Models;
using System.Windows.Input;
using Xamarin.Forms;

namespace SeeZeero.UI.ViewModels
{
    public class FileDetailViewModel : BaseViewModel
    {
        public ICommand OnCloseView { get; set; }
        INavigation _navigation;
        UserFiles _fileData;
        public UserFiles FileData { get { return _fileData; }  set{ SetProperty(ref _fileData, value); }  }
        public FileDetailViewModel(INavigation navigationPage)
        {
            _navigation = navigationPage;
            OnCloseView = new Command(async () =>
            {
                await _navigation.PopAllPopupAsync();
            });
        }
    }
}

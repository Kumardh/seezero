﻿using SeeZeero.UI.Models;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace SeeZeero.UI.ViewModels
{
    public class MyFoldersViewModel : CommonViewModel
    {
        private ObservableCollection<UserFiles> userFiles;

        public ObservableCollection<UserFiles> FilesList
        {
            get { return userFiles; }
            set { SetProperty(ref userFiles, value); }
        }
        string folderOwnerName;
        public string FolderName { get { return folderOwnerName; } set { SetProperty(ref folderOwnerName, value); } }


        private string folderDescription;

        public string FolderDescription
        {
            get { return folderDescription; }
            set { SetProperty(ref folderDescription, value); }
        }

        public ICommand OnFileSelect { get; set; }
        INavigation _navigationPage;
        public MyFoldersViewModel(INavigation navigationPage) : base(navigationPage)
		{
			_navigationPage = navigationPage;
            FilesList = new ObservableCollection<UserFiles>
            {
                new UserFiles
                {
                    FileName="Collections.pdf",
                    FileType = FileType.PDF,
                    FileDate = DateTime.Now.AddDays(-5),
                        SharedBy ="Test User",
                    SharedTo ="Test User 1",
                    FileSize="10.3 MB"
                },
                new UserFiles
                {
                    FileName="Bank Statement.pdf",
                    FileType = FileType.PDF,
                    FileDate = DateTime.Now.AddDays(-15),
                        SharedBy ="Test User",
                    SharedTo ="Test User 1",
                    FileSize="10.3 MB"
                },
                new UserFiles
                {
                    FileName="Resume.pdf",
                    FileType = FileType.PDF,
                    FileDate = DateTime.Now.AddDays(-3),
                        SharedBy ="Test User",
                    SharedTo ="Test User 1",
                    FileSize="10.3 MB"
                },
                new UserFiles
                {
                    FileName="Project Report.pdf",
                    FileType = FileType.PDF,
                    FileDate = DateTime.Now.AddDays(-5),
                        SharedBy ="Test User",
                    SharedTo ="Test User 1",
                    FileSize="10.3 MB"
                },
                new UserFiles
                {
                    FileName="Suggestions.docx",
                    FileType = FileType.MS_WORD,
                    FileDate = DateTime.Now.AddDays(-20),
                        SharedBy ="Test User",
                    SharedTo ="Test User 1",
                    FileSize="10.3 MB"
                },
                new UserFiles
                {
                    FileName="Company Presentation.pptx",
                    FileType = FileType.MS_PPT,
                    FileDate = DateTime.Now.AddDays(-25),
                        SharedBy ="Test User",
                    SharedTo ="Test User 1",
                    FileSize="10.3 MB"
                },
                new UserFiles
                {
                    FileName="Project Thesis.pdf",
                    FileType = FileType.PDF,
                    FileDate = DateTime.Now.AddDays(-35),
                        SharedBy ="Test User",
                    SharedTo ="Test User 1",
                    FileSize="10.3 MB"
                },
                new UserFiles
                {
                    FileName="Website Data.doc",
                    FileType = FileType.MS_WORD,
                    FileDate = DateTime.Now.AddDays(-5),
                        SharedBy ="Test User",
                    SharedTo ="Test User 1",
                    FileSize="10.3 MB"
                },
                new UserFiles
                {
                    FileName="Passport Photo.png",
                    FileType = FileType.IMAGE,
                    FileDate = DateTime.Now.AddDays(-5),
                        SharedBy ="Test User",
                    SharedTo ="Test User 1",
                    FileSize="10.3 MB"
                },
            };
            OnFileSelect = new Command<UserFiles>((userFile) =>
            {
                var selectedFile = userFile;
                MessagingCenter.Send<CommonViewModel, object>(this, "DisplayActionSheet", userFile);

            });
        }
        public void ShowUserFiles(UserFolder userFolder)
        {
            FolderName = (userFolder.UserType == UserType.SELF ? "My " : userFolder.FolderName + "'s ") + "Folder";
            FolderDescription = "Edocs " + (userFolder.UserType == UserType.SELF ? "uploaded by you " : " Sent by " + userFolder.FolderName);
            FilesList = new ObservableCollection<UserFiles>
            {
                new UserFiles
                {
                    FileName="Resume.pdf",
                    FileType = FileType.PDF,
                    FileDate = DateTime.Now.AddDays(-3),
                    SharedBy ="Test User",
                    SharedTo ="Test User 1",
                    FileSize="10.3 MB"
                },
                new UserFiles
                {
                    FileName="Project Report.pdf",
                    FileType = FileType.PDF,
                    FileDate = DateTime.Now.AddDays(-5),
                    SharedBy ="Test User 2",
                    SharedTo ="Test User 11",
                    FileSize="110.65 MB"
                },
                new UserFiles
                {
                    FileName="Suggestions.docx",
                    FileType = FileType.MS_WORD,
                    FileDate = DateTime.Now.AddDays(-20),
                    SharedBy ="Test User",
                    SharedTo ="Test User 1",
                    FileSize="10.3 MB"
                },
                new UserFiles
                {
                    FileName="Company Presentation.pptx",
                    FileType = FileType.MS_PPT,
                    FileDate = DateTime.Now.AddDays(-25),
                        SharedBy ="Test User",
                    SharedTo ="Test User 1",
                    FileSize="10.3 MB"
                },

                new UserFiles
                {
                    FileName="Website Data.doc",
                    FileType = FileType.MS_WORD,
                    FileDate = DateTime.Now.AddDays(-5),
                        SharedBy ="Test User",
                    SharedTo ="Test User 1",
                    FileSize="10.3 MB"
                },
                new UserFiles
                {
                    FileName="Passport Photo.png",
                    FileType = FileType.IMAGE,
                    FileDate = DateTime.Now.AddDays(-5),
                        SharedBy ="Test User",
                    SharedTo ="Test User 1",
                    FileSize="10.3 MB"
                },
            };
        }

    }
}

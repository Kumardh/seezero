﻿using Rg.Plugins.Popup.Extensions;
using SeeZeero.UI.Models;
using System.Windows.Input;
using Xamarin.Forms;

namespace SeeZeero.UI.ViewModels
{
    public class CommonViewModel : BaseViewModel
    {
        INavigation _navigationPage;
        public ICommand OnMyEDocsClick { get; set; }
        public ICommand OnMyConnectionsClick { get; set; }
        public ICommand OnShareQRCodeClick { get; set; }
        public ICommand OnNotificationClick { get; set; }
        public ICommand OnShareFile { get; set; }
        public CommonViewModel(INavigation navigation):this()
        {
            _navigationPage = navigation;
            OnShareFile = new Command(async () =>
            {
                if(_navigationPage?.ModalStack?.Count>0)
                    await _navigationPage.PopAllPopupAsync();

                MessagingCenter.Send<CommonViewModel, object>(this, "DisplayFileShareAlert1", null);
            });
        }
        public CommonViewModel()
        {
            OnMyEDocsClick = new Command(() =>
            {
                MessagingCenter.Send<CommonViewModel, PageNames>(this, "Navigate", PageNames.MyDocuments);
            });
            OnMyConnectionsClick = new Command(() =>
            {
                MessagingCenter.Send<CommonViewModel, PageNames>(this, "Navigate", PageNames.MyConnections);
            });
            OnShareQRCodeClick = new Command(() =>
            {
                MessagingCenter.Send<CommonViewModel, PageNames>(this, "Navigate", PageNames.ShareQRCode);
            });
            OnNotificationClick = new Command(() =>
            {
                MessagingCenter.Send<CommonViewModel, PageNames>(this, "Navigate", PageNames.Notifications);
            });
        }
    }
}

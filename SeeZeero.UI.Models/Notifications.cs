﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeeZeero.UI.Models
{
    public class Notifications
    {
        public string ConnectionName { get; set; }
        public ConnectionStatus ConnectionStatus { get; set; }
        public string ConnectionStatusDisplay
        {
            get {
                string status = "";
                switch (ConnectionStatus)
                {
                    case ConnectionStatus.RECIEVED:
                        status = "Has sent you a connection request";
                        break;
                    case ConnectionStatus.ACCEPTED:
                        status = "Accepted your connection request";
                        break;
                    case ConnectionStatus.REJECTED:
                        status = "Has cancel request";
                        break;
                    case ConnectionStatus.PENDING:
                        status = "Your connection request is pending";
                        break;
                    case ConnectionStatus.EXPIRED:
                        status = "Your connection request has expired";
                        break;
                    case ConnectionStatus.FILE_SHARED:
                        status = "Has sent a new file to you";
                        break;
                    case ConnectionStatus.FILE_REQUESTED:
                        status = "Has request a file from you";
                        break;
                    default:
                        break;
                }
                return status;
            }
        }

        public string AcceptIcon
        {
            get { return "Assets/acpt.png"; }
        }
        public string DenyIcon
        {
            get { return "Assets/dny.png"; }
        }
        public bool ShowActions { get { return ConnectionStatus == ConnectionStatus.RECIEVED; } }
    }
}

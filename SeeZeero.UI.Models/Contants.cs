﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeeZeero.UI.Models
{
    public enum UserType
    {
        SELF,
        OTHER
    }
    public  enum ConnectionStatus
    {
        RECIEVED,
        ACCEPTED,
        REJECTED,
        PENDING,
        EXPIRED,
        FILE_SHARED,
        FILE_REQUESTED
    }
   public enum PageNames
    {
        MyDocuments,
        MyConnections,
        ShareQRCode,
        Notifications
    }
    public enum FileType
    {
        MS_WORD,
        MS_EXCEL,
        MS_PPT,
        TXT,
        PDF,
        IMAGE,
        AUDIO,
        VIDEO,
        ZIP,
        RTF,
        APPLICATION
    }
}

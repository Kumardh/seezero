﻿namespace SeeZeero.UI.Models
{
    public class UserDetail
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeeZeero.UI.Models
{
    public class Connections
    {
        public string ConnectionIcon { get; set; }
        public string ConnectionName { get; set; }
        public string MoreIcon { get { return "Assets/link.png"; } }
        public string RequestButton { get; set; }

        public bool IsMore { get; set; }
        public bool IsButton { get; set; }
    }
}

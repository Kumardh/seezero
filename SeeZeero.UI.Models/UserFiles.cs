﻿using System;

namespace SeeZeero.UI.Models
{
    public class UserFiles
    {
        public FileType FileType { get; set; }
        public string FileName { get; set; }
        public DateTime FileDate { get; set; }
        public string SharedBy { get; set; }
        public string SharedTo { get; set; }

        public string SharedToDisplay { get { return "Share With: " + SharedTo; } }
        public string SharedByDisplay { get { return "Shared By: " + SharedBy; } }
        public string FileDateDisplayDetail { get
            {
                return "Time Uploaded: " + FileDate.ToString("MM-dd-yy hh:mmtt");
            }
        }
        public string FileDateDisplay { get
            {
                return "Uploaded on " + FileDate.ToString("MMM dd yyyy") + " at " + FileDate.ToString("hh:mm tt") ;
            }
        }
        public string FileIcon
        {

            get
            {
                string icon = "Assets/word.png";
                switch (FileType)
                {
                    case FileType.MS_WORD:
                        icon = "Assets/word.png";
                        break;
                    case FileType.MS_EXCEL:
                        icon = "Assets/word.png";
                        break;
                    case FileType.MS_PPT:
                        icon = "Assets/p_pont.png";
                        break;
                    case FileType.TXT:
                        icon = "Assets/word.png";
                        break;
                    case FileType.PDF:
                        icon = "Assets/pdf.png";
                        break;
                    case FileType.IMAGE:
                        icon = "Assets/thum_img.png";
                        break;
                    case FileType.AUDIO:
                        icon = "Assets/word.png";
                        break;
                    case FileType.VIDEO:
                        icon = "Assets/word.png";
                        break;
                    case FileType.ZIP:
                        icon = "Assets/word.png";
                        break;
                    case FileType.RTF:
                        icon = "Assets/word.png";
                        break;
                    case FileType.APPLICATION:
                        icon = "Assets/word.png";
                        break;
                    default:
                        break;
                }
                return icon;
            }
        }

        public string FileSizeDisplay { get { return "Size: " + FileSize; } }
        public string FileSize { get; set; }
        public string LinkIcon
        {
            get
            {
                return "Assets/link.png";
            }
        }
    }
}

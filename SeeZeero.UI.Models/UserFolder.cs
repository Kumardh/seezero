﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeeZeero.UI.Models
{
    public class UserFolder
    {
        public string FolderIcon { get { return "Assets/folder.png"; } }
        public string FolderName { get; set; }
        public string MoreIcon { get { return "Assets/next_arow.png"; } }
        public UserType UserType { get; set; }
    }
}
